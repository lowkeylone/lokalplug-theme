$(function () {
    adjustContentContainerHeight();
    $('[data-toggle="popover"]').popover();

    let randomPlugPlayerVolumeStepsCount = $('.media-player-volume-container').width()/6;
    let randomPlugPlayerVolume = new VolumeControl('#randomPlugPlayerVolume', randomPlugPlayerVolumeStepsCount);
    randomPlugPlayerVolume.renderUI(VolumeControl.getDefaultVolumeValue());
});

$(window).resize(function() {
    adjustContentContainerHeight();
});

$('.popover-dismiss').popover({
    trigger: 'focus'
});

function adjustContentContainerHeight() {
    $('.main-container .content').css("min-height", $(window).height()-196 + "px" );
}

$('.mixtape-track').click(function () {
    $(document.body).find('.mixtape-track').removeClass('playing');
    $(document.body).find('.track-volume-container').empty();
    $(document.body).find('.track-progress-bar').hide();
    $(this).addClass('playing');
    $(this).find('.track-progress-bar').show();
    let $volumeContainer = $(this).find('.track-volume-container');

    $("<div class='volume'><input type='range' id='trackPlayerVolume' class='volume'/></div>").appendTo($volumeContainer);

    let trackPlayerVolumeStepsCount = $('.track-volume-container').width()/6;
    let trackPlayerVolume = new VolumeControl("#trackPlayerVolume", trackPlayerVolumeStepsCount);
    trackPlayerVolume.renderUI(VolumeControl.getDefaultVolumeValue());
}).children().click(function (e) {
    let className = e.target.className;
    return !_.includes(className, 'volume') && !_.includes(className, 'fa-') && !_.includes(className, 'progress');
});

class VolumeControl {
    static getDefaultVolumeValue() {
        return 0.6;
    }

    constructor(selector, stepsCount) {
        this.$input = $(selector);
        this.$input.hide();
        this.steps = stepsCount;

        if (this.steps > 10)
            this.steps = 10;

        this.$slider = $("<div class='volume-slider'><div class='volume-slider-bar'></div><ul class='volume-slider-sticks'></div>").appendTo(this.$input.parent());

        for (let i = 0; i < this.steps && i < (this.$input.parent().width()-5)/6; i++) {
            let $stick = $('<li><div class="volume-slider-stick"></div></li>').appendTo(this.$slider.find('.volume-slider-sticks'));
            $stick.on('mouseenter', function () {
                $(this).addClass('active');
            }).on('mouseleave', function () {
                $(this).removeClass('active');
            });
        }

        this.$slider.on('mousedown', this, VolumeControl.onDragEvent);
    }

    static onDragEvent(event) {
        let volumeControl = event.data;
        volumeControl.renderUI(volumeControl.getPercent(event));
        $(document.body).on('mousemove', function (event) {
            volumeControl.renderUI(volumeControl.getPercent(event));
        });
        $(document.body).on('mouseup', function () {
            $(document.body).off('mouseup');
            $(document.body).off('mousemove');
        });
    }

    renderUI(percent) {
        let index = Math.round(percent * this.steps);
        index = index < this.steps ? index : this.steps;
        this.$slider.find('.volume-slider-sticks > li').find('div').css('opacity', 0);

        for (let i = 0; i < index; i++) {
            this.$slider.find('.volume-slider-sticks > li:eq(' + i + ')').find('div').css('opacity', 1);
        }
    }

    getPercent(event) {
        let percent = (event.pageX - this.$slider.offset().left) / this.$slider.find('.volume-slider-sticks').width();
        percent = percent >= 0 ? percent : 0;
        percent = percent <= 1 ? percent : 1;
        return percent;
    }
}