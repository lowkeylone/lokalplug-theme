<?php
$title = "Mixtapes - Lokal Plug";
$navActiveIndex = 1;
require 'template/header.php';

function includeMixtapeCard($title, $artist, $poster, $avatar)
{
    $mixtapeTitle = $title;
    $mixtapeArtist = $artist;
    $mixtapePoster = $poster;
    $mixtapeArtistAvatar = $avatar;

    include 'template/mixtape-card.php';
}

?>

    <div class="container-fluid main-container">
        <div class="col-lg-8 col-lg-offset-2 content">
            <div class="col-xs-12 col-sm-10">
                <div class="container content-container">
                    <div class="row">
                        <div class="page-header section-title">
                            <h1><span class="fulltape fulltape-icon"></span> Mixtapes
                                <small>Browse all available mixtapes and more..</small>
                            </h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-5">
                            <div class="col-xs-12">
                                <ul class="list-inline list-filters">
                                    <li>
                                        <span class="label filter active">All mixtapes</span>
                                    </li>
                                    <li>
                                        <span class="label filter">Only artists</span>
                                    </li>
                                    <li>
                                        <span class="label filter">Only DJs</span>
                                    </li>
                                    <li>
                                        <span class="label filter">Only beatmakers</span>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-xs-12">
                                <ul class="list-inline list-filters">
                                    <li>
                                        <span class="label filter active">All mixtapes</span>
                                    </li>
                                    <li>
                                        <span class="label filter">Only officials</span>
                                    </li>
                                    <li>
                                        <span class="label filter">Only non-officials</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-7">
                            <ul class="list-inline list-sort-dropdowns">
                                <li>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default btn-xs">Latest</button>
                                        <button type="button" class="btn btn-default btn-xs dropdown-toggle"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><span class="sort-filter">Most rated</span></li>
                                            <li><span class="sort-filter">Most downloaded</span></li>
                                        </ul>
                                    </div>
                                </li>
                                <li>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default btn-xs">Any time</button>
                                        <button type="button" class="btn btn-default btn-xs dropdown-toggle"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Today</a></li>
                                            <li><a href="#">This week</a></li>
                                            <li><a href="#">Last 2 weeks</a></li>
                                            <li><a href="#">This month</a></li>
                                            <li><a href="#">This year</a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default btn-xs">Any music genre</button>
                                        <button type="button" class="btn btn-default btn-xs dropdown-toggle"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Rap/Trap</a></li>
                                            <li><a href="#">Dancehall</a></li>
                                            <li><a href="#">Reggae</a></li>
                                            <li><a href="#">Zouk</a></li>
                                            <li><a href="#">Other</a></li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="jumbotron">
                            <div class="row card-list-container">
                                <div class="col-sm-2 col-xs-4">
                                    <?php includeMixtapeCard("Street Monopoly Origins", "Bruce Little", "street_monopoly_origins-bruce_little.png", "brucelittle.jpg") ?>
                                </div>
                                <div class="col-sm-2 col-xs-4">
                                    <?php includeMixtapeCard("Young Drug Dealers", "Tiitof", "ydd-tiitof.png", "tiitof.jpg") ?>
                                </div>
                                <div class="col-sm-2 col-xs-4">
                                    <?php includeMixtapeCard("Gzup 2", "Ed Style", "gzup_mixtape_2-ed_style.jpg", "edstyle.jpg") ?>
                                </div>
                                <div class="col-sm-2 col-xs-4">
                                    <?php includeMixtapeCard("True Story", "Ed Style", "true_story-ed_style.jpg", "edstyle.jpg") ?>
                                </div>
                                <div class="col-sm-2 col-xs-4">
                                    <?php includeMixtapeCard("Blackaman Back", "Black Sayko", "blackaman_back-black_sayko.png", "blacksayko.jpg") ?>
                                </div>
                                <div class="col-sm-2 col-xs-4">
                                    <?php includeMixtapeCard("Coca No Cola", "Jesus Bricks", "coca_no_cola-jesus_bricks.png", "jesusbricks.jpg") ?>
                                </div>
                                <div class="col-sm-2 col-xs-4">
                                    <?php includeMixtapeCard("Street Monopoly Origins", "Bruce Little", "street_monopoly_origins-bruce_little.png", "brucelittle.jpg") ?>
                                </div>
                                <div class="col-sm-2 col-xs-4">
                                    <?php includeMixtapeCard("Young Drug Dealers", "Tiitof", "ydd-tiitof.png", "tiitof.jpg") ?>
                                </div>
                                <div class="col-sm-2 col-xs-4">
                                    <?php includeMixtapeCard("Gzup 2", "Ed Style", "gzup_mixtape_2-ed_style.jpg", "edstyle.jpg") ?>
                                </div>
                                <div class="col-sm-2 col-xs-4">
                                    <?php includeMixtapeCard("True Story", "Ed Style", "true_story-ed_style.jpg", "edstyle.jpg") ?>
                                </div>
                                <div class="col-sm-2 col-xs-4">
                                    <?php includeMixtapeCard("Blackaman Back", "Black Sayko", "blackaman_back-black_sayko.png", "blacksayko.jpg") ?>
                                </div>
                                <div class="col-sm-2 col-xs-4">
                                    <?php includeMixtapeCard("Coca No Cola", "Jesus Bricks", "coca_no_cola-jesus_bricks.png", "jesusbricks.jpg") ?>
                                </div>
                                <div class="col-sm-2 col-xs-4">
                                    <?php includeMixtapeCard("Street Monopoly Origins", "Bruce Little", "street_monopoly_origins-bruce_little.png", "brucelittle.jpg") ?>
                                </div>
                                <div class="col-sm-2 col-xs-4">
                                    <?php includeMixtapeCard("Young Drug Dealers", "Tiitof", "ydd-tiitof.png", "tiitof.jpg") ?>
                                </div>
                                <div class="col-sm-2 col-xs-4">
                                    <?php includeMixtapeCard("Gzup 2", "Ed Style", "gzup_mixtape_2-ed_style.jpg", "edstyle.jpg") ?>
                                </div>
                                <div class="col-sm-2 col-xs-4">
                                    <?php includeMixtapeCard("True Story", "Ed Style", "true_story-ed_style.jpg", "edstyle.jpg") ?>
                                </div>
                                <div class="col-sm-2 col-xs-4">
                                    <?php includeMixtapeCard("Blackaman Back", "Black Sayko", "blackaman_back-black_sayko.png", "blacksayko.jpg") ?>
                                </div>
                                <div class="col-sm-2 col-xs-4">
                                    <?php includeMixtapeCard("Coca No Cola", "Jesus Bricks", "coca_no_cola-jesus_bricks.png", "jesusbricks.jpg") ?>
                                </div>
                                <div class="col-sm-2 col-xs-4">
                                    <?php includeMixtapeCard("Street Monopoly Origins", "Bruce Little", "street_monopoly_origins-bruce_little.png", "brucelittle.jpg") ?>
                                </div>
                                <div class="col-sm-2 col-xs-4">
                                    <?php includeMixtapeCard("Young Drug Dealers", "Tiitof", "ydd-tiitof.png", "tiitof.jpg") ?>
                                </div>
                                <div class="col-sm-2 col-xs-4">
                                    <?php includeMixtapeCard("Gzup 2", "Ed Style", "gzup_mixtape_2-ed_style.jpg", "edstyle.jpg") ?>
                                </div>
                                <div class="col-sm-2 col-xs-4">
                                    <?php includeMixtapeCard("True Story", "Ed Style", "true_story-ed_style.jpg", "edstyle.jpg") ?>
                                </div>
                                <div class="col-sm-2 col-xs-4">
                                    <?php includeMixtapeCard("Blackaman Back", "Black Sayko", "blackaman_back-black_sayko.png", "blacksayko.jpg") ?>
                                </div>
                                <div class="col-sm-2 col-xs-4">
                                    <?php includeMixtapeCard("Coca No Cola", "Jesus Bricks", "coca_no_cola-jesus_bricks.png", "jesusbricks.jpg") ?>
                                </div>
                            </div>
                            <nav aria-label="Page navigation" class="text-center">
                                <ul class="pagination">
                                    <li class="disabled">
                                        <a href="#" aria-label="Previous">
                                            <span aria-hidden="true">&laquo;</span>
                                        </a>
                                    </li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#">5</a></li>
                                    <li>
                                        <a href="#" aria-label="Next">
                                            <span aria-hidden="true">&raquo;</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <?php require 'template/sidebar.php'; ?>
        </div>
    </div>

<?php require 'template/footer.php'; ?>