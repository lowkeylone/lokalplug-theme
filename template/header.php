<?php
if (!isset($title))
    $title = "Lokal Plug - The Caribbean Mixtape Supplier";
if (!isset($navActiveIndex))
    $navActiveIndex = 0;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title; ?></title>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="styles/common.css">
    <link rel="stylesheet" type="text/css" href="styles/layout.css">
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>

<div class="container-fluid top-nav">
    <div class="col-lg-8 col-lg-offset-2">
        <ul class="list-inline">
            <li><a href="./signup-page.php"><i class="fa fa-user-plus" aria-hidden="true"></i> Signup</a></li>
            <li><a href="./login-page.php"><i class="fa fa-sign-in" aria-hidden="true"></i> Login</a></li>
            <li>
                <div class="dropdown languages-dropdown">
                    <a class="dropdown-toggle" type="button" data-toggle="dropdown">
                        Language
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">English <span class="flag-icon flag-icon-gb"></span></a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">French <span class="flag-icon flag-icon-fr"></a></li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</div>

<div class="container-fluid media-bar">
    <div class="col-sm-5 col-sm-offset-1 col-lg-4 col-lg-offset-2 text-center">
        <a href="./"><img class="brand-logo" alt="Lokal Plug" src="images/logo-text.png"></a>
    </div>
    <div class="col-xs-12 col-sm-4 media-player-container">
        <div class="media-player container-fluid">
            <div class="col-xs-3 col-sm-2 left-container">
                <a href="#"><img alt="Cover" src="images/samples/street_monopoly_origins-bruce_little.png"></a>
            </div>
            <div class="col-xs-9 col-sm-10 right-container">
                <a tabindex="0" class="media-player-info" role="button" data-placement="bottom" data-toggle="popover"
                   data-trigger="focus"
                   data-content="Here is the Plug Player. It play in random order the 100 best songs based on users votes."><i
                            class="fa fa-question-circle" aria-hidden="true"></i></a>
                <p class="song-info"><a href="#"><span id="current-song-title">Bruce Little - SKLM</span></a></p>
                <p class="song-info"><a href="#"><span id="current-song-mixtape">Street Monopoly Origin</span></a></p>
                <div class="col-xs-1 media-player-controls"><a href="#"><i class="fa fa-play" aria-hidden="true"></i></a></div>
                <div class="col-xs-6 media-player-controls">
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width: 25%" aria-valuenow="25"
                             aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
                <div class="col-xs-3 media-player-controls media-player-volume-container">
                    <div class="volume">
                        <input type="range" id="randomPlugPlayerVolume" class="volume"/>
                    </div>
                </div>
                <div class="col-xs-1 media-player-controls"><a href="#"><i class="fa fa-step-backward"
                                                                   aria-hidden="true"></i></a></div>
                <div class="col-xs-1 media-player-controls"><a href="#"><i class="fa fa-step-forward"
                                                                   aria-hidden="true"></i></a></div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid navbar-container">
    <div class="col-lg-8 col-lg-offset-2 col-xs-12">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#"><img alt="Brand" src="images/logo-text.png" height="100%"></a>
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li <?php if($navActiveIndex == 0) echo("class=\"active\"") ?>><a href="./">Home</a></li>
                        <li <?php if($navActiveIndex == 1) echo("class=\"active\"") ?>><a href="./mixtapes-list-page.php">Mixtapes</a></li>
                        <li <?php if($navActiveIndex == 2) echo("class=\"active\"") ?>><a href="./artists-list-page.php">Artists</a></li>
                        <li <?php if($navActiveIndex == 3) echo("class=\"active\"") ?>><a href="./contact-page.php">Contact</a></li>
                    </ul>

                    <form class="navbar-form navbar-right" role="search">
                        <div class="input-group">
                            <input class="form-control" placeholder="Search mixtapes, artists.." type="text">
                            <div class="input-group-btn">
                                <button class="btn btn-default" type="submit"><i class="fa fa-search"
                                                                                 aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </nav>
    </div>
</div>