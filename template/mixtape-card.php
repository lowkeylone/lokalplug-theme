<?php
if (!isset($mixtapePoster)) $mixtapePoster = "";
if (!isset($mixtapeTitle)) $mixtapeTitle = "";
if (!isset($mixtapeArtist)) $mixtapeArtist = "";
if (!isset($mixtapeArtistAvatar)) $mixtapeArtistAvatar = "";
?>

<div class="thumbnail list-card mixtape-card official-mixtape">
    <div class="card-cover">
        <a href="#">
            <div class="card-cover-bg"
                 style="background-image: url('images/samples/<?php echo $mixtapePoster ?>');"></div>
            <img alt="mixtape_cover" src="images/samples/<?php echo $mixtapePoster ?>">
        </a>
    </div>
    <div class="caption">
        <p class="card-title"><a href="#"><?php echo $mixtapeTitle ?></a></p>
        <p class="card-artist">
            <a href="#">
                <span class="avatar">
                    <img src="images/samples/<?php echo $mixtapeArtistAvatar ?>"><?php echo $mixtapeArtist ?>
                </span>
            </a>
        </p>
        <hr/>
        <div class="mixtape-labels">
            <span class="label label-success label-official">Official</span>
            <span class="label label-default label-non-official">Non Official</span>
        </div>
        <p style="font-size:10px"><i class="fa fa-clock-o"
                                     aria-hidden="true"></i> 9 months ago
        </p>
        <p style="font-size:10px"><i class="fa fa-cloud-download"
                                     aria-hidden="true"></i> 8833 d/l</p>
        <p style="font-size:10px">
            <i class="fa fa-star" aria-hidden="true"></i>
            <i class="fa fa-star" aria-hidden="true"></i>
            <i class="fa fa-star" aria-hidden="true"></i>
            <i class="fa fa-star-half" aria-hidden="true"></i>
            <i class="fa fa-star-o" aria-hidden="true"></i>
        </p>
    </div>
</div>