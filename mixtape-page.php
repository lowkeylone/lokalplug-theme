<?php
$title = "Street Monopoly Origins - Lokal Plug";
$navActiveIndex = 1;
require 'template/header.php';
?>

    <div class="container-fluid main-container">
        <div class="col-lg-8 col-lg-offset-2 content">
            <div class="col-xs-12 col-sm-10">
                <div class="content-container">
                    <div class="mixtape-infos">
                        <div class="mixtape-infos-background"
                             style="background: url('images/samples/street_monopoly_origins-bruce_little.png') 100px;background-size: 100px;"></div>
                        <div class="col-sm-3 mixtape-cover-container">
                            <img src="images/samples/street_monopoly_origins-bruce_little.png">
                        </div>
                        <div class="col-sm-9 mixtape-infos-container">
                            <h3 class="mixtape-title">Street Monopoly Origins</h3>
                            <p><a href="#"><span class="avatar"><img
                                                src="images/samples/brucelittle.jpg">Bruce Little</span></a></p>
                            <div class="col-sm-4 col-md-3 mixtape-infos-column">
                                <p><a class="featuring-artists-popover"
                                      tabindex="0"
                                      data-html="true"
                                      data-toggle="popover"
                                      data-trigger="focus"
                                      data-placement="bottom"
                                      title="Featuring Artists"
                                      data-content='<ul class=&quot;list-inline featuring-artists&quot;>
                                <li>
                                    <a href=&quot;#&quot;><span class=&quot;avatar&quot;><img
                                                    src=&quot;images/samples/edstyle.jpg&quot;>Ed Style</span></a>
                                </li>
                                <li>
                                    <a href=&quot;#&quot;><span class=&quot;avatar&quot;><img
                                                    src=&quot;images/default-avatar.png&quot;>Riino</span></a>
                                </li>
                                <li>
                                    <a href=&quot;#&quot;><span class=&quot;avatar&quot;><img src=&quot;images/samples/lyrrix.jpg&quot;>Lyrrix</span></a>
                                </li>
                                <li>
                                    <a href=&quot;#&quot;><span class=&quot;avatar&quot;><img src=&quot;images/samples/mercenaire.jpg&quot;>Mercenaire</span></a>
                                </li>
                                <li>
                                    <a href=&quot;#&quot;><span class=&quot;avatar&quot;><img
                                                    src=&quot;images/samples/madness.jpg&quot;>Madness</span></a>
                                </li>
                                <li>
                                    <a href=&quot;#&quot;><span class=&quot;avatar&quot;><img src=&quot;images/samples/kend.jpg&quot;>Ken&#39;D</span></a>
                                </li>
                                </ul>'><i class="fa fa-microphone" aria-hidden="true"></i> Featuring Artists</a></p>
                                <p><i class="fa fa-star" aria-hidden="true"></i> 3.5/5 stars</p>
                                <p><i class="fa fa-music" aria-hidden="true"></i> Rap/Trap</p>
                            </div>
                            <div class="col-sm-4 col-md-3 mixtape-infos-column">
                                <p><i class="fa fa-cloud-download" aria-hidden="true"></i> 8833 downloads</p>
                                <p><i class="fa fa-calendar" aria-hidden="true"></i> July 14, 2016</p>
                                <p><a href="#"><i class="fa fa-upload" aria-hidden="true"></i> le_xav</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="mixtape-toolbar">
                        <ul class="list-inline">
                            <li><a href="#"><h5><i class="fa fa-play" aria-hidden="true"></i> <span>Play</span></h5></a>
                            </li>
                            <li><a href="#"><h5><i class="fa fa-thumbs-up" aria-hidden="true"></i> <span>Like</span>
                                    </h5></a></li>
                            <li><a href="#"><h5><i class="fa fa-thumbs-down" aria-hidden="true"></i>
                                        <span>Dislike</span></h5></a></li>
                            <li><a href="#"><h5><i class="fa fa-heart" aria-hidden="true"></i> <span>Favorite</span>
                                    </h5></a></li>
                            <li><a href="#"><h5><i class="fa fa-arrow-down" aria-hidden="true"></i>
                                        <span>Download</span></h5></a></li>
                        </ul>
                    </div>
                    <div class="jumbotron">
                        <ul class="list-inline share-list text-right">
                            <li><a href="#" title="Share on Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            </li>
                            <li><a href="#" title="Share on Google+"><i class="fa fa-google-plus"
                                                                        aria-hidden="true"></i></a></li>
                            <li><a href="#" title="Share on Twitter"><i class="fa fa-twitter"
                                                                        aria-hidden="true"></i></a></li>
                            <li><a href="#" title="Share on Instagram"><i class="fa fa-instagram"
                                                                          aria-hidden="true"></i></a></li>
                        </ul>
                        <div class="section-title">
                            <h1><span class="fulltape fulltape-icon"></span> Tracklist
                            </h1>
                        </div>
                        <ul class="list-group">
                            <li class="list-group-item mixtape-track">
                                <div class="col-xs-1 track-number">1.</div>
                                <div class="col-xs-5 col-sm-7 track-title">
                                    <i class="fa fa-play" aria-hidden="true"></i>
                                    <i class="fa fa-volume-up" aria-hidden="true"></i>
                                    <i class="fa fa-pause" aria-hidden="true"></i>
                                    Intro Prod. Stephen G
                                </div>
                                <div class="col-xs-6 col-sm-4 track-controls-container">
                                    <div class="col-xs-10 track-play-controls">
                                        <div class="col-xs-6 progress-bar-container">
                                            <div class="progress track-progress-bar">
                                                <div class="progress-bar" role="progressbar" style="width: 25%"
                                                     aria-valuenow="25"
                                                     aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 track-volume-container"></div>
                                    </div>
                                    <div class="col-xs-2">
                                        <a href="#"><i class="fa fa-arrow-down" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item mixtape-track">
                                <div class="col-xs-1 track-number">2.</div>
                                <div class="col-xs-5 col-sm-7 track-title">
                                    <i class="fa fa-play" aria-hidden="true"></i>
                                    <i class="fa fa-volume-up" aria-hidden="true"></i>
                                    <i class="fa fa-pause" aria-hidden="true"></i>
                                    Look At Me Prod. Stephen G
                                </div>
                                <div class="col-xs-6 col-sm-4 track-controls-container">
                                    <div class="col-xs-10 track-play-controls">
                                        <div class="col-xs-6 progress-bar-container">
                                            <div class="progress track-progress-bar">
                                                <div class="progress-bar" role="progressbar" style="width: 25%"
                                                     aria-valuenow="25"
                                                     aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 track-volume-container"></div>
                                    </div>
                                    <div class="col-xs-2">
                                        <a href="#"><i class="fa fa-arrow-down" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item mixtape-track">
                                <div class="col-xs-1 track-number">3.</div>
                                <div class="col-xs-5 col-sm-7 track-title">
                                    <i class="fa fa-play" aria-hidden="true"></i>
                                    <i class="fa fa-volume-up" aria-hidden="true"></i>
                                    <i class="fa fa-pause" aria-hidden="true"></i>
                                    Ouais ouais Prod. Waly Beats
                                </div>
                                <div class="col-xs-6 col-sm-4 track-controls-container">
                                    <div class="col-xs-10 track-play-controls">
                                        <div class="col-xs-6 progress-bar-container">
                                            <div class="progress track-progress-bar">
                                                <div class="progress-bar" role="progressbar" style="width: 25%"
                                                     aria-valuenow="25"
                                                     aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 track-volume-container"></div>
                                    </div>
                                    <div class="col-xs-2">
                                        <a href="#"><i class="fa fa-arrow-down" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item mixtape-track">
                                <div class="col-xs-1 track-number">4.</div>
                                <div class="col-xs-5 col-sm-7 track-title">
                                    <i class="fa fa-play" aria-hidden="true"></i>
                                    <i class="fa fa-volume-up" aria-hidden="true"></i>
                                    <i class="fa fa-pause" aria-hidden="true"></i>
                                    Lozeil ft. Ed Style, Madness Prod. Lethal Track
                                </div>
                                <div class="col-xs-6 col-sm-4 track-controls-container">
                                    <div class="col-xs-10 track-play-controls">
                                        <div class="col-xs-6 progress-bar-container">
                                            <div class="progress track-progress-bar">
                                                <div class="progress-bar" role="progressbar" style="width: 25%"
                                                     aria-valuenow="25"
                                                     aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 track-volume-container"></div>
                                    </div>
                                    <div class="col-xs-2">
                                        <a href="#"><i class="fa fa-arrow-down" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item mixtape-track">
                                <div class="col-xs-1 track-number">5.</div>
                                <div class="col-xs-5 col-sm-7 track-title">
                                    <i class="fa fa-play" aria-hidden="true"></i>
                                    <i class="fa fa-volume-up" aria-hidden="true"></i>
                                    <i class="fa fa-pause" aria-hidden="true"></i>
                                    Adam ft. Riino Prod. Young Bvngz
                                </div>
                                <div class="col-xs-6 col-sm-4 track-controls-container">
                                    <div class="col-xs-10 track-play-controls">
                                        <div class="col-xs-6 progress-bar-container">
                                            <div class="progress track-progress-bar">
                                                <div class="progress-bar" role="progressbar" style="width: 25%"
                                                     aria-valuenow="25"
                                                     aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 track-volume-container"></div>
                                    </div>
                                    <div class="col-xs-2">
                                        <a href="#"><i class="fa fa-arrow-down" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item mixtape-track">
                                <div class="col-xs-1 track-number">6.</div>
                                <div class="col-xs-5 col-sm-7 track-title">
                                    <i class="fa fa-play" aria-hidden="true"></i>
                                    <i class="fa fa-volume-up" aria-hidden="true"></i>
                                    <i class="fa fa-pause" aria-hidden="true"></i>
                                    Balmain ft. Lyrrix, Ken'D Prod. Logo Beat
                                </div>
                                <div class="col-xs-6 col-sm-4 track-controls-container">
                                    <div class="col-xs-10 track-play-controls">
                                        <div class="col-xs-6 progress-bar-container">
                                            <div class="progress track-progress-bar">
                                                <div class="progress-bar" role="progressbar" style="width: 25%"
                                                     aria-valuenow="25"
                                                     aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 track-volume-container"></div>
                                    </div>
                                    <div class="col-xs-2">
                                        <a href="#"><i class="fa fa-arrow-down" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item mixtape-track">
                                <div class="col-xs-1 track-number">7.</div>
                                <div class="col-xs-5 col-sm-7 track-title">
                                    <i class="fa fa-play" aria-hidden="true"></i>
                                    <i class="fa fa-volume-up" aria-hidden="true"></i>
                                    <i class="fa fa-pause" aria-hidden="true"></i>
                                    Nèf ft. Mercenaire Prod. Young Bvngz
                                </div>
                                <div class="col-xs-6 col-sm-4 track-controls-container">
                                    <div class="col-xs-10 track-play-controls">
                                        <div class="col-xs-6 progress-bar-container">
                                            <div class="progress track-progress-bar">
                                                <div class="progress-bar" role="progressbar" style="width: 25%"
                                                     aria-valuenow="25"
                                                     aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 track-volume-container"></div>
                                    </div>
                                    <div class="col-xs-2">
                                        <a href="#"><i class="fa fa-arrow-down" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item mixtape-track">
                                <div class="col-xs-1 track-number">8.</div>
                                <div class="col-xs-5 col-sm-7 track-title">
                                    <i class="fa fa-play" aria-hidden="true"></i>
                                    <i class="fa fa-volume-up" aria-hidden="true"></i>
                                    <i class="fa fa-pause" aria-hidden="true"></i>
                                    LMAO Prod. CashMoneyAP
                                </div>
                                <div class="col-xs-6 col-sm-4 track-controls-container">
                                    <div class="col-xs-10 track-play-controls">
                                        <div class="col-xs-6 progress-bar-container">
                                            <div class="progress track-progress-bar">
                                                <div class="progress-bar" role="progressbar" style="width: 25%"
                                                     aria-valuenow="25"
                                                     aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 track-volume-container"></div>
                                    </div>
                                    <div class="col-xs-2">
                                        <a href="#"><i class="fa fa-arrow-down" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item mixtape-track">
                                <div class="col-xs-1 track-number">9.</div>
                                <div class="col-xs-5 col-sm-7 track-title">
                                    <i class="fa fa-play" aria-hidden="true"></i>
                                    <i class="fa fa-volume-up" aria-hidden="true"></i>
                                    <i class="fa fa-pause" aria-hidden="true"></i>
                                    SKLM Prod. Young Bvngz
                                </div>
                                <div class="col-xs-6 col-sm-4 track-controls-container">
                                    <div class="col-xs-10 track-play-controls">
                                        <div class="col-xs-6 progress-bar-container">
                                            <div class="progress track-progress-bar">
                                                <div class="progress-bar" role="progressbar" style="width: 25%"
                                                     aria-valuenow="25"
                                                     aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 track-volume-container"></div>
                                    </div>
                                    <div class="col-xs-2">
                                        <a href="#"><i class="fa fa-arrow-down" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <?php require 'template/sidebar.php'; ?>
        </div>
    </div>

<?php require 'template/footer.php'; ?>