<?php
$title = "Artists - Lokal Plug";
$navActiveIndex = 2;
require 'template/header.php';

function includeArtistCard($artistName, $avatar)
{
    $artist = $artistName;
    $artistAvatar = $avatar;

    include 'template/artist-card.php';
}
?>

<div class="container-fluid main-container">
    <div class="col-lg-8 col-lg-offset-2 content">
        <div class="col-xs-12 col-sm-10">
            <div class="container content-container">
                <div class="row">
                    <div class="page-header section-title">
                        <h1><span class="fulltape fulltape-icon"></span> Artists
                            <small>Browse all artists and DJs</small>
                        </h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-5">
                        <ul class="list-inline list-filters">
                            <li><span class="label filter active">All artists/DJs</span></li>
                            <li><span class="label filter">Only artists</span></li>
                            <li><span class="label filter">Only DJs</span></li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-md-7">
                        <ul class="list-inline list-sort-dropdowns">
                            <li>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-xs">Recent activity</button>
                                    <button type="button" class="btn btn-default btn-xs dropdown-toggle"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Most popular mixtapes</a></li>
                                        <li><a href="#">Most rated mixtapes</a></li>
                                        <li><a href="#">Most downloaded mixtapes</a></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="jumbotron">
                        <div class="row card-list-container">
                            <div class="col-sm-2 col-xs-4">
                                <?php includeArtistCard("Bruce Little", "brucelittle.jpg") ?>
                            </div>
                            <div class="col-sm-2 col-xs-4">
                                <?php includeArtistCard("Tiitof", "tiitof.jpg") ?>
                            </div>
                            <div class="col-sm-2 col-xs-4">
                                <?php includeArtistCard("Ed Style", "edstyle.jpg") ?>
                            </div>
                            <div class="col-sm-2 col-xs-4">
                                <?php includeArtistCard("Black Sayko", "blacksayko.jpg") ?>
                            </div>
                            <div class="col-sm-2 col-xs-4">
                                <?php includeArtistCard("Mercenaire", "mercenaire.jpg") ?>
                            </div>
                            <div class="col-sm-2 col-xs-4">
                                <?php includeArtistCard("W.Bricks", "wbricks.jpg") ?>
                            </div>
                            <div class="col-sm-2 col-xs-4">
                                <?php includeArtistCard("Jesus Bricks", "jesusbricks.jpg") ?>
                            </div>
                            <div class="col-sm-2 col-xs-4">
                                <?php includeArtistCard("Ken'D", "kend.jpg") ?>
                            </div>
                            <div class="col-sm-2 col-xs-4">
                                <?php includeArtistCard("Lyrrix", "lyrrix.jpg") ?>
                            </div>
                            <div class="col-sm-2 col-xs-4">
                                <?php includeArtistCard("Madness", "madness.jpg") ?>
                            </div>
                            <div class="col-sm-2 col-xs-4">
                                <?php includeArtistCard("Bruce Little", "brucelittle.jpg") ?>
                            </div>
                            <div class="col-sm-2 col-xs-4">
                                <?php includeArtistCard("Tiitof", "tiitof.jpg") ?>
                            </div>
                            <div class="col-sm-2 col-xs-4">
                                <?php includeArtistCard("Ed Style", "edstyle.jpg") ?>
                            </div>
                            <div class="col-sm-2 col-xs-4">
                                <?php includeArtistCard("Black Sayko", "blacksayko.jpg") ?>
                            </div>
                            <div class="col-sm-2 col-xs-4">
                                <?php includeArtistCard("Mercenaire", "mercenaire.jpg") ?>
                            </div>
                            <div class="col-sm-2 col-xs-4">
                                <?php includeArtistCard("W.Bricks", "wbricks.jpg") ?>
                            </div>
                            <div class="col-sm-2 col-xs-4">
                                <?php includeArtistCard("Jesus Bricks", "jesusbricks.jpg") ?>
                            </div>
                            <div class="col-sm-2 col-xs-4">
                                <?php includeArtistCard("Ken'D", "kend.jpg") ?>
                            </div>
                        </div>

                        <nav aria-label="Page navigation" class="text-center">
                            <ul class="pagination">
                                <li class="disabled">
                                    <a href="#" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                    </a>
                                </li>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li>
                                    <a href="#" aria-label="Next">
                                        <span aria-hidden="true">&raquo;</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <?php require 'template/sidebar.php'; ?>
    </div>
</div>

<?php require 'template/footer.php'; ?>
