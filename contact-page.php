<?php
$title = "Contact - Lokal Plug";
$navActiveIndex = 3;
require 'template/header.php';
?>

<div class="container-fluid main-container">
    <div class="col-lg-8 col-lg-offset-2 content">
        <div class="col-xs-12">
            <div class="content-container">
                <div class="page-header section-title">
                    <h1><span class="fulltape fulltape-icon"></span> Contact
                        <small>Whether you are an artist or a music lover create your account now</small>
                    </h1>
                </div>
                <div class="form-container contact-form-container">
                    <form role="form">
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Name">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Subject">
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <textarea class="form-control" rows="5" placeholder="Message"></textarea>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="col-xs-12 col-md-4 col-md-offset-4">
                                <div class="form-group">
                                    <input type="submit" class="btn btn-default g-recaptcha"
                                           data-sitekey="6LfHpSIUAAAAANUSsfx6ViMYX5M6cK7yxST9IfTd"
                                           data-badge="inline"
                                           data-callback="YourOnSubmitFn" value="Submit">
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4 text-right">
                                <img src="images/recaptcha.png" alt="Protected by reCaptcha" width="75px" height="22px">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require 'template/footer.php'; ?>
