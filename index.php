<?php
function includeMixtapeCard($title, $artist, $poster, $avatar) {
    $mixtapeTitle = $title;
    $mixtapeArtist = $artist;
    $mixtapePoster = $poster;
    $mixtapeArtistAvatar = $avatar;

    include 'template/mixtape-card.php';
}

require 'template/header.php';
?>

    <div class="container-fluid main-container">
        <div class="col-lg-8 col-lg-offset-2 content">
            <div class="col-xs-12 col-sm-10">
                <div class="container content-container">
                    <div class="row">
                        <div class="page-header section-title">
                            <h1><span class="fulltape fulltape-icon"></span> Trending Mixtapes
                                <small>The mixtapes being streamed the most in the last 5 minutes</small>
                            </h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="jumbotron">
                            <div class="row card-list-container">
                                <div class="col-sm-2 col-xs-4">
                                    <?php includeMixtapeCard("Street Monopoly Origins", "Bruce Little", "street_monopoly_origins-bruce_little.png", "brucelittle.jpg") ?>
                                </div>
                                <div class="col-sm-2 col-xs-4">
                                    <?php includeMixtapeCard("Young Drug Dealers", "Tiitof", "ydd-tiitof.png", "tiitof.jpg") ?>
                                </div>
                                <div class="col-sm-2 col-xs-4">
                                    <?php includeMixtapeCard("Gzup 2", "Ed Style", "gzup_mixtape_2-ed_style.jpg", "edstyle.jpg") ?>
                                </div>
                                <div class="col-sm-2 col-xs-4">
                                    <?php includeMixtapeCard("True Story", "Ed Style", "true_story-ed_style.jpg", "edstyle.jpg") ?>
                                </div>
                                <div class="col-sm-2 col-xs-4">
                                    <?php includeMixtapeCard("Blackaman Back", "Black Sayko", "blackaman_back-black_sayko.png", "blacksayko.jpg") ?>
                                </div>
                                <div class="col-sm-2 col-xs-4">
                                    <?php includeMixtapeCard("Coca No Cola", "Jesus Bricks", "coca_no_cola-jesus_bricks.png", "jesusbricks.jpg") ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="page-header section-title">
                            <h1><span class="fulltape fulltape-icon"></span> Latest Official Mixtapes
                                <small>The latest official mixtapes from major artists</small>
                            </h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="jumbotron">
                            <div class="row card-list-container">
                                <div class="col-sm-2 col-xs-4">
                                    <?php includeMixtapeCard("Street Monopoly Origins", "Bruce Little", "street_monopoly_origins-bruce_little.png", "brucelittle.jpg") ?>
                                </div>
                                <div class="col-sm-2 col-xs-4">
                                    <?php includeMixtapeCard("Young Drug Dealers", "Tiitof", "ydd-tiitof.png", "tiitof.jpg") ?>
                                </div>
                                <div class="col-sm-2 col-xs-4">
                                    <?php includeMixtapeCard("Gzup 2", "Ed Style", "gzup_mixtape_2-ed_style.jpg", "edstyle.jpg") ?>
                                </div>
                                <div class="col-sm-2 col-xs-4">
                                    <?php includeMixtapeCard("True Story", "Ed Style", "true_story-ed_style.jpg", "edstyle.jpg") ?>
                                </div>
                                <div class="col-sm-2 col-xs-4">
                                    <?php includeMixtapeCard("Blackaman Back", "Black Sayko", "blackaman_back-black_sayko.png", "blacksayko.jpg") ?>
                                </div>
                                <div class="col-sm-2 col-xs-4">
                                    <?php includeMixtapeCard("Coca No Cola", "Jesus Bricks", "coca_no_cola-jesus_bricks.png", "jesusbricks.jpg") ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="page-header section-title">
                            <h1><span class="fulltape fulltape-icon"></span> Latest Uploads
                                <small>The latest mixtapes uploaded, official or not</small>
                            </h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="jumbotron">
                            <div class="row card-list-container">
                                <div class="col-sm-2 col-xs-4">
                                    <?php includeMixtapeCard("Street Monopoly Origins", "Bruce Little", "street_monopoly_origins-bruce_little.png", "brucelittle.jpg") ?>
                                </div>
                                <div class="col-sm-2 col-xs-4">
                                    <?php includeMixtapeCard("Young Drug Dealers", "Tiitof", "ydd-tiitof.png", "tiitof.jpg") ?>
                                </div>
                                <div class="col-sm-2 col-xs-4">
                                    <?php includeMixtapeCard("Gzup 2", "Ed Style", "gzup_mixtape_2-ed_style.jpg", "edstyle.jpg") ?>
                                </div>
                                <div class="col-sm-2 col-xs-4">
                                    <?php includeMixtapeCard("True Story", "Ed Style", "true_story-ed_style.jpg", "edstyle.jpg") ?>
                                </div>
                                <div class="col-sm-2 col-xs-4">
                                    <?php includeMixtapeCard("Blackaman Back", "Black Sayko", "blackaman_back-black_sayko.png", "blacksayko.jpg") ?>
                                </div>
                                <div class="col-sm-2 col-xs-4">
                                    <?php includeMixtapeCard("Coca No Cola", "Jesus Bricks", "coca_no_cola-jesus_bricks.png", "jesusbricks.jpg") ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php require 'template/sidebar.php'; ?>
        </div>
    </div>

<?php require 'template/footer.php'; ?>